import React from 'react';
import { Image, StyleSheet, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';
import images from '../constants/images';

const Message = ({ src, messages }) => (
  <View style={styles.container}>
    <Image source={src} style={styles.img} />
    {messages !== 0 ? (
      <View style={styles.notification}>
        <Text style={styles.text}>{messages > 5 ? '5+' : messages}</Text>
      </View>
    ) : null}
  </View>
);

Message.propTypes = {
  messages: PropTypes.number,
  src: PropTypes.number,
};

Message.defaultProps = {
  messages: 0,
  src: images.messageIcon,
};

const styles = StyleSheet.create({
  container: {
    height: scale(15),
    width: scale(18),
    position: 'relative',
  },
  img: {
    height: scale(15),
    width: scale(18),
  },
  notification: {
    position: 'absolute',
    top: scale(-9),
    right: scale(-9),
    width: scale(18),
    height: scale(18),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.today,
    borderRadius: scale(9),
  },
  text: {
    color: 'white',
    ...fonts.t9,
  },
});

export default Message;
