const tabNavigatorLabel = '#a7a7a7';
const shadowHeader = 'rgba(0, 0, 0, 0.05)';
const shadowRaiting = 'rgba(133, 140, 152, 0.1)';
const postBackground = '#f6f8fa';
const resolve = '#82c9d2';
const info = '#666666';
const address = '#323648';
const days = '#afafaf';
const plumber = '#85c9d1';
const dots = 'white';
const today = '#f0560f';
const buttonBorder = '#e4e6ec';
const linearGradient = ['rgba(50, 54, 73, 0)', '#323649'];

const colors = {
  tabNavigatorLabel,
  shadowHeader,
  postBackground,
  resolve,
  info,
  address,
  days,
  plumber,
  dots,
  shadowRaiting,
  today,
  buttonBorder,
  linearGradient,
};

export default colors;
