import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import images from '../constants/images';
import { scale } from '../utilites/scale';
import fonts from '../assets/fonts/fonts';
import { sections } from '../screens/Dashboard';
import ArrowButton from './ArrowButton';

class ScrollToMonth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      months: [],
    };
  }

  componentDidMount() {
    this.setState({ months: sections.map(({ title }) => title) });
  }

  nextMonth = () => {
    const { index, months } = this.state;
    const { scroll } = this.props;
    this.setState({
      index: index === months.length - 1 ? months.length - 1 : (this.state.index += 1),
    });
    scroll(index + 1);
  };

  prevMonth = () => {
    const { index } = this.state;
    const { scroll } = this.props;
    this.setState({ index: index === 0 ? 0 : (this.state.index -= 1) });
    scroll(this.state.index);
  };

  render() {
    const { months, index } = this.state;
    return (
      <View style={styles.time}>
        <ArrowButton image={images.arrowLeft} pressFunc={this.prevMonth} />
        <Text style={styles.timeText}>{months[index]}</Text>
        <ArrowButton image={images.arrowRight} pressFunc={this.nextMonth} />
      </View>
    );
  }
}

ScrollToMonth.propTypes = {
  scroll: PropTypes.func,
};

ScrollToMonth.defaultProps = {
  scroll: () => {},
};

const styles = StyleSheet.create({
  time: {
    width: scale(104),
    height: scale(26),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  timeText: {
    ...fonts.t2,
  },
});

export default ScrollToMonth;
