import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

const Spacer = ({ height }) => <View style={{ height, width: '100%' }} />;

Spacer.propTypes = {
  height: PropTypes.number,
};

Spacer.defaultProps = {
  height: 10,
};

export default Spacer;
