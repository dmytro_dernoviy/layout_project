import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import images from '../constants/images';
import fonts from '../assets/fonts/fonts';

const Clock = ({ time }) => {
  return (
    <View style={styles.clockContainer}>
      <Image source={images.clockIcon} style={styles.imgClock} />
      <Text style={styles.time}>{moment(time).format('HH A')}</Text>
    </View>
  );
};

Clock.propTypes = {
  time: PropTypes.string,
};

Clock.defaultProps = {
  time: '',
};

const styles = StyleSheet.create({
  clockContainer: {
    height: scale(12),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  imgClock: {
    width: scale(9),
    height: scale(9),
    marginRight: scale(5),
  },
  time: {
    color: colors.tabNavigatorLabel,
    ...fonts.t4,
  },
});

export default Clock;
