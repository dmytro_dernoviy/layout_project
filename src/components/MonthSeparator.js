import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import Spacer from './Spacer';
import fonts from '../assets/fonts/fonts';

const MonthSeparator = ({ title }) => (
  <View style={styles.container}>
    <Spacer height={scale(20)} />
    <Text style={styles.text}>{title}</Text>
  </View>
);

MonthSeparator.propTypes = {
  title: PropTypes.string,
};

MonthSeparator.defaultProps = {
  title: '',
};

const styles = StyleSheet.create({
  container: {
    marginLeft: scale(45),
  },
  text: {
    color: colors.days,
    ...fonts.t8,
    lineHeight: scale(15),
    opacity: 0.5,
  },
});

export default MonthSeparator;
