import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';

const PostContainer = ({ children }) => <View style={styles.container}>{children}</View>;

PostContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = StyleSheet.create({
  container: {
    width: scale(270),
    paddingTop: scale(17),
    paddingBottom: scale(20),
    paddingLeft: scale(20),
    paddingRight: scale(20),
    backgroundColor: colors.postBackground,
    borderRadius: scale(5),
  },
});

export default PostContainer;
