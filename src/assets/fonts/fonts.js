import { scale } from '../../utilites/scale';

const fonts = {
  t1: {
    fontFamily: 'Roboto',
    fontSize: scale(26),
  },
  t2: {
    fontFamily: 'Roboto',
    fontSize: scale(14),
  },
  t3: {
    fontFamily: 'CarosSoftBold',
    fontSize: scale(18),
  },
  t4: {
    fontFamily: 'CarosSoftMedium',
    fontSize: scale(10),
  },
  t5: {
    fontFamily: 'CarosSoftBold',
    fontSize: scale(12),
  },
  t6: {
    fontFamily: 'CarosSoft',
    fontSize: scale(10),
  },
  t7: {
    fontFamily: 'CarosSoftBold',
    fontSize: scale(14),
  },
  t8: {
    fontFamily: 'CarosSoftMedium',
    fontSize: scale(12),
  },
  t9: {
    fontFamily: 'CarosSoftBold',
    fontSize: scale(10),
  },
};

export default fonts;
