import React from 'react';
import { SafeAreaView, StyleSheet, SectionList } from 'react-native';
import Header from '../components/Header';
import data from '../../mokedData';
import EventsByDate from '../components/EventsByDate';
import groupForSectionList from '../utilites/groupForSectionList';
import renderItemByType from '../components/renderItem';
import addDateSeparator from '../utilites/addDateSeparatorToData';
import { validate } from '../utilites/validateData';
import InvalidData from '../components/InvalidData';

const dataWithSeparator = addDateSeparator(data);
export const sections = groupForSectionList(dataWithSeparator);
const isValid = validate(data);

class DashboardScreen extends React.Component {
  constructor() {
    super();
    this.list = React.createRef();
  }

  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <Header list={this.list} />
        {isValid ? (
          <SectionList
            ref={this.list}
            sections={sections}
            renderItem={({ item }) => renderItemByType(item)}
            renderSectionHeader={({ section: { title } }) => <EventsByDate date={title} />}
            keyExtractor={(item, index) => item + index}
            onScrollToIndexFailed={() => {}}
            style={styles.list}
            stickySectionHeadersEnabled={false}
          />
        ) : (
          <InvalidData />
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    width: '85%',
    marginLeft: '7.5%',
  },
  safeArea: {
    flex: 1,
  },
});

export default DashboardScreen;
