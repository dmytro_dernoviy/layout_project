import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';
import Spacer from './Spacer';

const OrangeButton = ({ widthButton }) => (
  <React.Fragment>
    <Spacer height={scale(15)} />
    <TouchableOpacity style={{ ...styles.button, width: widthButton }}>
      <Text style={styles.text}>Rearrage</Text>
    </TouchableOpacity>
  </React.Fragment>
);

OrangeButton.propTypes = {
  widthButton: PropTypes.number,
};

OrangeButton.defaultProps = {
  widthButton: 100,
};

const styles = StyleSheet.create({
  button: {
    height: scale(38),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(5),
    backgroundColor: colors.today,
  },
  text: {
    color: 'white',
    ...fonts.t8,
  },
});

export default OrangeButton;
