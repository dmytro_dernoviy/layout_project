import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import Clock from './Clock';
import colors from '../constants/colors';
import PostContainer from './PostContainer';
import Spacer from './Spacer';
import fonts from '../assets/fonts/fonts';

const Reminder1 = ({ content, date }) => (
  <PostContainer>
    <View style={styles.header}>
      <Text style={styles.title}>{content?.title}</Text>
      <Clock time={date} />
    </View>
    <Spacer height={scale(10)} />
    <Text style={styles.info}>{content?.content}</Text>
    <Spacer height={scale(2)} />
    <Text style={styles.price}>
      {content?.price_currency}
      {content?.price}
    </Text>
  </PostContainer>
);

Reminder1.propTypes = {
  content: PropTypes.shape({
    title: PropTypes.string,
    content: PropTypes.string,
    price_currency: PropTypes.string,
    price: PropTypes.number,
  }),
  date: PropTypes.string,
};

Reminder1.defaultProps = {
  content: {
    title: '',
    content: '',
    price_currency: '',
    price: 0,
  },
  date: '',
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    ...fonts.t5,
  },
  info: {
    ...fonts.t4,
    color: colors.info,
  },
  price: {
    ...fonts.t3,
  },
});

export default Reminder1;
