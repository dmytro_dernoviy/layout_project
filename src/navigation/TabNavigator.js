import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import React from 'react';
import DashboardScreen from '../screens/Dashboard';
import FindScreen from '../screens/Find';
import ActivityScreen from '../screens/Activity';
import MessageScreen from '../screens/Message';
import ProfileScreen from '../screens/Profile';
import images from '../constants/images';
import TabImage from '../components/TabIcon';
import colors from '../constants/colors';
import TabBar from '../components/TabBar';
import Message from '../components/Message';

const TabNavigator = createBottomTabNavigator(
  {
    Dashboard: {
      screen: DashboardScreen,
      navigationOptions: {
        tabBarIcon: () => <TabImage src={images.dashboardIcon} />,
      },
    },
    Find: {
      screen: FindScreen,
      navigationOptions: {
        tabBarIcon: () => <TabImage src={images.findIcon} />,
      },
    },
    Activity: {
      screen: ActivityScreen,
      navigationOptions: {
        tabBarIcon: () => <TabImage src={images.activityIcon} />,
      },
    },
    Message: {
      screen: MessageScreen,
      navigationOptions: {
        tabBarIcon: () => <Message src={images.messageIcon} messages={2} />,
      },
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        tabBarIcon: () => <TabImage src={images.profileIcon} />,
      },
    },
  },
  {
    tabBarComponent: TabBar,
    tabBarOptions: {
      inactiveTintColor: colors.tabNavigatorLabel,
    },
  }
);

export default createAppContainer(TabNavigator);
