import { compareDate } from './compareDate';

const groupForSectionList = function groupBy(arr) {
  return Object.values(
    arr.reduce((aggregate, item) => {
      const ag = { ...aggregate };
      const val = compareDate(item.create_at);
      if (!aggregate[val]) {
        ag[val] = {
          title: val,
          data: [],
        };
      }
      ag[val].data.push(item);
      return ag;
    }, {})
  );
};

export default groupForSectionList;
