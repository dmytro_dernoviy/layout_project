import { scale } from '../utilites/scale';

const main = scale(30);
const post = scale(20);

const padding = {
  main,
  post,
};

export default padding;
