import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';

const Customers = ({ landlord, customers }) => (
  <View style={styles.container}>
    <Image source={landlord} style={[styles.landlord, styles.customers]} />
    <View style={styles.customerCont}>
      {customers?.map((item, i) => (
        <Image
          source={item?.avatar}
          style={i === 0 ? styles.customers : [styles.customers, styles.cover]}
          key={item?.id}
        />
      ))}
    </View>
  </View>
);

Customers.propTypes = {
  landlord: PropTypes.number,
  customers: PropTypes.arrayOf(PropTypes.any),
};

Customers.defaultProps = {
  landlord: null,
  customers: [],
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  landlord: {
    marginRight: scale(8),
  },
  customers: {
    height: scale(28),
    width: scale(28),
    borderRadius: scale(13),
  },
  customerCont: {
    flexDirection: 'row',
  },
  cover: {
    position: 'relative',
    right: scale(10),
    zIndex: -1,
  },
});

export default Customers;
