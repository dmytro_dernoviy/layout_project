import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import colors from '../constants/colors';
import { scale } from '../utilites/scale';
import fonts from '../assets/fonts/fonts';

const AppartamentInfo = ({ data }) => (
  <View style={styles.container}>
    {data?.map((item, index) => (
      <View style={styles.block} key={item}>
        <Text style={styles.text}>{item}</Text>
        {index === data.length - 1 ? null : <Text style={styles.dot}>{'\u2B24'}</Text>}
      </View>
    ))}
  </View>
);

AppartamentInfo.propTypes = {
  data: PropTypes.arrayOf(PropTypes.string),
};

AppartamentInfo.defaultProps = {
  data: [],
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  text: {
    color: colors.info,
    ...fonts.t6,
    lineHeight: scale(12),
  },
  dot: {
    marginLeft: scale(4),
    marginRight: scale(4),
    color: colors.buttonBorder,
    fontSize: scale(3),
  },
  point: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  block: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default AppartamentInfo;
