import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';

const SliderInfo = ({ data }) => {
  const { price, price_currency, price_description, apartment_description } = data || {};
  const priceInfo = price ? `: ${price_currency}${price} ` : '';
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        {apartment_description}
        <Text style={styles.price}>{priceInfo}</Text>
        {price_description}
      </Text>
    </View>
  );
};

SliderInfo.propTypes = {
  data: PropTypes.shape({
    apartment_description: PropTypes.string,
    price: PropTypes.number,
    price_currency: PropTypes.string,
    price_description: PropTypes.string,
  }),
};

SliderInfo.defaultProps = {
  data: {
    apartment_description: '',
    price: 0,
    price_currency: '',
    price_description: '',
  },
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
  },
  text: {
    color: colors.info,
    ...fonts.t6,
    lineHeight: scale(13),
  },
  price: {
    ...fonts.t9,
    color: colors.address,
    lineHeight: scale(13),
  },
});

export default SliderInfo;
