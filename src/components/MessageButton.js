import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';
import Spacer from './Spacer';

const MessageButton = ({ widthButton }) => (
  <React.Fragment>
    <Spacer height={scale(15)} />
    <TouchableOpacity style={{ ...styles.button, width: widthButton }}>
      <Text style={styles.text}>Message Landlord</Text>
    </TouchableOpacity>
  </React.Fragment>
);

MessageButton.propTypes = {
  widthButton: PropTypes.number,
};

MessageButton.defaultProps = {
  widthButton: 100,
};

const styles = StyleSheet.create({
  button: {
    height: scale(38),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(5),
    borderWidth: scale(1),
    borderColor: colors.buttonBorder,
    backgroundColor: 'white',
  },
  text: {
    color: colors.today,
    ...fonts.t8,
  },
});

export default MessageButton;
