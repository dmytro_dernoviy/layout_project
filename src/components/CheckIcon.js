import React from 'react';
import { Image, StyleSheet } from 'react-native';
import images from '../constants/images';
import { scale } from '../utilites/scale';

const CheckedIcon = () => <Image source={images.checkIcon} style={styles.imgCheck} />;

const styles = StyleSheet.create({
  imgCheck: {
    width: scale(8),
    height: scale(5),
  },
});

export default CheckedIcon;
