const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sut'];

export const getDay = date => {
  const day = new Date(date);
  return { dayNumber: day.getDate(), dayName: dayNames[day.getDay()] };
};
export const getMonth = date => new Date(date).getMonth();
export const getFullYear = date => new Date(date).getFullYear();
export const getHours = date => {
  const result = new Date(date).getHours();
  return result;
};
