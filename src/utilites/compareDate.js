export const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const compareDate = data => {
  const presentDay = new Date('2018-09-03T09:00:00.000Z');
  const tomorrow = new Date(presentDay.getTime() + 24 * 60 * 60 * 1000);
  const mokedDate = new Date(data);
  if (presentDay.getTime() > mokedDate.getTime()) {
    return 'Past events';
  }
  if (tomorrow.getDate() === mokedDate.getDate()) {
    return 'Tomorrow';
  }
  if (presentDay.getTime() === mokedDate.getTime()) {
    return 'Today';
  }
  if (presentDay.getFullYear() < mokedDate.getFullYear()) {
    const monthWithYear = `${monthNames[mokedDate.getMonth()]}, ${mokedDate.getFullYear()}`;
    return monthWithYear;
  }
  return monthNames[mokedDate.getMonth()];
};
