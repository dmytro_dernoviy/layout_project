import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import PostContainer from './PostContainer';
import Clock from './Clock';
import fonts from '../assets/fonts/fonts';

const Reminder4 = ({ data }) => (
  <PostContainer>
    <View style={styles.block}>
      <Text style={styles.text}>{data?.content_object?.title}</Text>
      <Clock time={data?.create_at} />
    </View>
  </PostContainer>
);

Reminder4.propTypes = {
  data: PropTypes.shape({
    content_object: PropTypes.shape({
      title: PropTypes.string,
    }),
    create_at: PropTypes.string,
  }),
};

Reminder4.defaultProps = {
  data: {
    content_object: {
      title: '',
    },
    create_at: '',
  },
};

const styles = StyleSheet.create({
  text: {
    ...fonts.t5,
  },
  block: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default Reminder4;
