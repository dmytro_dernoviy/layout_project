import React from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import SlidePostHeader from './SlidePostHeader';
import SliderInfo from './SliderInfo';
import Address from './Address';
import AppartamentInfo from './AppartamentsInfo';
import Customers from './Customers';
import Carosuel from './Carosuel';
import PostContainer from './PostContainer';
import Spacer from './Spacer';
import RenderButton from './RenderButton';
import images from '../constants/images';

const SlidePost = ({ data }) => {
  const { apartment, category } = data?.content_object;
  // eslint-disable-next-line no-shadow
  const { images, address, appartment_options, customers } = apartment;
  return (
    <PostContainer>
      <SlidePostHeader style={styles.header} data={data} />
      <Spacer height={scale(10)} />
      <Carosuel data={images} />
      <Spacer height={scale(6)} />
      <SliderInfo data={apartment} />
      <Spacer height={scale(2)} />
      <Address address={address} />
      <Spacer height={scale(3)} />
      <AppartamentInfo data={appartment_options} />
      <Spacer height={scale(15)} />
      <Customers landlord={apartment?.landlord?.avatar} customers={customers} />
      <RenderButton category={category} />
    </PostContainer>
  );
};

SlidePost.propTypes = {
  data: PropTypes.shape({
    content_object: PropTypes.shape({
      category: PropTypes.string,
      apartment: PropTypes.shape({
        address: PropTypes.string,
        appartment_options: PropTypes.arrayOf(PropTypes.string),
        images: PropTypes.arrayOf(PropTypes.any),
        price_currency: PropTypes.string,
        landlord: PropTypes.shape({
          avatar: PropTypes.number,
        }),
        customers: PropTypes.arrayOf(PropTypes.any),
      }),
    }),
  }),
};

SlidePost.defaultProps = {
  data: {
    content_object: {
      category: '',
      apartment: {
        address: '',
        appartment_options: [],
        images: [],
        landlord: {
          avatar: images.customer,
        },
        customers: [],
        apartment_description: '',
        price: '',
        price_currency: '',
        price_description: '',
      },
    },
  },
};

const styles = StyleSheet.create({
  header: {
    shadowOffset: { width: scale(3), height: scale(15) },
    shadowColor: colors.shadowHeader,
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
});

export default SlidePost;
