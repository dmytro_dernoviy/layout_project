import React from 'react';
import PropTypes from 'prop-types';
import OrangeButton from './OrangeButton';
import MessageButton from './MessageButton';
import { scale } from '../utilites/scale';

const RenderButton = ({ category }) => {
  switch (category) {
    case 'Check-in':
      return <OrangeButton widthButton={scale(132)} />;
    case 'Check-out':
      return <MessageButton widthButton={scale(160)} />;
    default:
      return null;
  }
};

RenderButton.propTypes = {
  category: PropTypes.string,
};

RenderButton.defaultProps = {
  category: '',
};

export default RenderButton;
