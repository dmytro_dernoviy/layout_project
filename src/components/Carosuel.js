import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';

export default class Carosuel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
      screens: [],
    };
  }

  componentDidMount() {
    const { data } = this.props;
    this.setState({ screens: data?.map(item => item) });
  }

  renderItem({ item }) {
    return <Image source={item} style={styles.img} />;
  }

  render() {
    const { screens, activeTab } = this.state;
    return (
      <View style={styles.container}>
        <Carousel
          data={screens}
          renderItem={this.renderItem}
          onSnapToItem={i => this.setState({ activeTab: i })}
          sliderWidth={scale(230)}
          itemWidth={scale(230)}
          slideStyle={{ width: scale(230) }}
          containerCustomStyle={{ borderRadius: scale(5) }}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          loop
        />
        <View style={styles.tabBar}>
          <LinearGradient colors={colors.linearGradient} style={styles.gradient} />
          <Pagination
            dotsLength={screens?.length}
            containerStyle={styles.pagination}
            dotStyle={styles.ww}
            inactiveDotOpacity={0.5}
            inactiveDotScale={1}
            activeDotIndex={activeTab}
          />
        </View>
      </View>
    );
  }
}

Carosuel.propTypes = {
  data: PropTypes.arrayOf(PropTypes.number),
};

Carosuel.defaultProps = {
  data: [],
};

const styles = StyleSheet.create({
  ww: {
    width: scale(6),
    height: scale(6),
    backgroundColor: colors.dots,
    borderRadius: scale(5),
  },
  tabBar: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    left: 0,
  },
  img: {
    width: scale(230),
    height: scale(140),
    borderRadius: scale(5),
  },
  pagination: {
    paddingVertical: 0,
    paddingTop: scale(20),
    paddingBottom: scale(14),
  },
  gradient: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    left: 0,
    top: 0,
    borderBottomLeftRadius: scale(5),
    borderBottomRightRadius: scale(5),
    opacity: 0.5,
  },
});
