import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import fonts from '../assets/fonts/fonts';
import colors from '../constants/colors';
import { scale } from '../utilites/scale';

const InvalidData = () => (
  <View style={styles.container}>
    <Text style={styles.text}>Invalid data</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    width: '90%',
    marginLeft: '5%',
    marginTop: scale(30),
    height: scale(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.resolve,
    borderRadius: scale(10),
  },
  text: {
    ...fonts.t1,
    color: colors.today,
  },
});

export default InvalidData;
