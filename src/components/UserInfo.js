import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import User from './User';
import Clock from './Clock';

const UserInfo = ({ position, date }) => (
  <View style={styles.container}>
    <User position={position} />
    <Clock time={date} />
  </View>
);

UserInfo.propTypes = {
  position: PropTypes.string,
  date: PropTypes.string,
};

UserInfo.defaultProps = {
  position: 'position',
  date: '',
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default UserInfo;
