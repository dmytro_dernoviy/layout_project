import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';

const ProfileComplete = ({ complete }) => (
  <View style={styles.container}>
    <View style={styles.completeContainer}>
      <View style={[styles.item, styles.first, styles.active]} />
      <View style={styles.item} />
      <View style={styles.item} />
      <View style={[styles.item, styles.last]} />
    </View>
    <Text style={styles.text}>{complete * 100}%</Text>
  </View>
);

ProfileComplete.propTypes = {
  complete: PropTypes.number,
};

ProfileComplete.defaultProps = {
  complete: 0.25,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    color: colors.today,
    ...fonts.t5,
  },
  completeContainer: {
    width: scale(190),
    height: scale(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item: {
    height: '100%',
    width: scale(46),
    backgroundColor: 'white',
  },
  active: {
    backgroundColor: colors.today,
  },
  first: {
    borderTopLeftRadius: scale(5),
    borderBottomLeftRadius: scale(5),
  },
  last: {
    borderTopRightRadius: scale(5),
    borderBottomLeftRadius: scale(5),
  },
});

export default ProfileComplete;
