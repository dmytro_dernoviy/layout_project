import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import fonts from '../assets/fonts/fonts';

const EventsByDate = ({ date }) => (
  <View>
    <Text style={styles.header}>{date}</Text>
  </View>
);

EventsByDate.propTypes = {
  date: PropTypes.string,
};

EventsByDate.defaultProps = {
  date: '',
};

const styles = StyleSheet.create({
  header: {
    marginTop: scale(30),
    ...fonts.t3,
  },
});

export default EventsByDate;
