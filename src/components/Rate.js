import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { scale } from '../utilites/scale';
import padding from '../constants/padding';
import colors from '../constants/colors';
import StarRaiting from './StarRating';
import Spacer from './Spacer';
import fonts from '../assets/fonts/fonts';

const Rate = () => (
  <View style={styles.container}>
    <Text style={styles.header}>Rate your experience</Text>
    <Spacer height={scale(9)} />
    <Text style={styles.info}>
      We value all feedback. Help us improve our services by rating your experience.
    </Text>
    <Spacer height={scale(20)} />
    <StarRaiting />
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: scale(270),
    padding: padding.post,
    justifyContent: 'space-between',
    shadowColor: colors.shadowRaiting,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 33,
    elevation: 1,
  },
  header: {
    ...fonts.t7,
  },
  info: {
    ...fonts.t6,
    lineHeight: scale(17),
  },
});

export default Rate;
