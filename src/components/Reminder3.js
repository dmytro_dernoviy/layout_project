import React from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import OrangeButton from './OrangeButton';
import images from '../constants/images';
import fonts from '../assets/fonts/fonts';

const Reminder3 = ({ data }) => (
  <ImageBackground
    source={images.reminderBackground}
    style={styles.img}
    imageStyle={{ borderRadius: scale(5) }}
  >
    <View style={styles.container}>
      <Text style={styles.text}>{data?.title}</Text>
      <OrangeButton widthButton={scale(90)} />
    </View>
  </ImageBackground>
);

Reminder3.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
  }),
};

Reminder3.defaultProps = {
  data: {
    title: '',
  },
};

const styles = StyleSheet.create({
  container: {
    width: scale(270),
    paddingTop: scale(32),
    paddingBottom: scale(30),
    paddingLeft: scale(30),
    paddingRight: scale(30),
  },
  text: {
    color: 'white',
    ...fonts.t3,
  },
  img: {
    width: scale(270),
    height: '100%',
  },
});

export default Reminder3;
