import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import images from '../constants/images';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';

const User = ({ position }) => (
  <View style={styles.container}>
    <Image source={images.avatarPlumber} style={styles.avatar} />
    <View style={styles.userBlock}>
      <Text style={styles.name}>Paul Smith</Text>
      <View style={styles.info}>
        <Text style={styles.plumber}>{position}</Text>
      </View>
    </View>
  </View>
);

User.propTypes = {
  position: PropTypes.string,
};

User.defaultProps = {
  position: 'position',
};

const styles = StyleSheet.create({
  container: {
    height: scale(40),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  avatar: {
    width: scale(40),
    height: scale(40),
    marginRight: scale(16),
  },
  userBlock: {
    height: scale(37),
    justifyContent: 'space-between',
  },
  info: {
    width: scale(52),
    height: scale(20),
    borderRadius: scale(5),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    ...fonts.t5,
  },
  plumber: {
    color: colors.plumber,
    ...fonts.t9,
  },
});

export default User;
