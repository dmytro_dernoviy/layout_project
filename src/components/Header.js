import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import padding from '../constants/padding';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';
// eslint-disable-next-line import/no-cycle
import ScrollToMonth from './ScrollToMonth';

const Header = ({ list }) => {
  const scroll = index => {
    list.current.scrollToLocation({
      animated: true,
      sectionIndex: index,
      itemIndex: 0,
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Your activity</Text>
      <ScrollToMonth scroll={scroll} />
    </View>
  );
};

Header.propTypes = {
  list: PropTypes.objectOf(PropTypes.any),
};

Header.defaultProps = {
  list: {},
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingLeft: padding.main,
    paddingRight: padding.main,
    paddingBottom: scale(23),
    paddingTop: scale(22),
    shadowColor: colors.shadowHeader,
    shadowOffset: { width: 0, height: scale(3) },
    shadowOpacity: 0.5,
    elevation: 5,
  },
  header: {
    ...fonts.t1,
  },
});

export default Header;
