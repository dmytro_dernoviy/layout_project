import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import FeedBackHeader from './FeedBackHeader';
import Rate from './Rate';

const FeedBack = ({ data }) => {
  const { user, issue } = data?.content_object;
  return (
    <View style={styles.container}>
      <FeedBackHeader position={user?.position} issue={issue?.text} date={data?.create_at} />
      <Rate />
    </View>
  );
};

FeedBack.propTypes = {
  data: PropTypes.shape({
    content_object: PropTypes.shape({
      user: PropTypes.shape({
        position: PropTypes.string,
      }),
      issue: PropTypes.shape({
        text: PropTypes.string,
      }),
    }),
    create_at: PropTypes.string,
  }),
};

FeedBack.defaultProps = {
  data: {
    content_object: {
      user: {
        position: '',
      },
      issue: {
        text: '',
      },
    },
    create_at: '',
  },
};

const styles = StyleSheet.create({
  container: {
    width: scale(270),
    borderRadius: scale(5),
  },
});

export default FeedBack;
