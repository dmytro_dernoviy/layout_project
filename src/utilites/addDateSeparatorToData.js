import { monthNames } from './compareDate';
import { getMonth, getDay } from './getDate';

const separateDate = (prev, next) => {
  const prevDate = `${monthNames[getMonth(prev)]} ${getDay(prev).dayNumber + 1}`;
  const nextDate = `${monthNames[getMonth(next)]} ${getDay(next).dayNumber - 1}`;
  return `${prevDate} - ${nextDate}`;
};

const validateResponse = data => data.filter(item => item);

const addDateSeparator = data => {
  const filterData = validateResponse(data);
  const newData = [];
  filterData.forEach((item, i) => {
    newData.push(item);
    if (i === filterData.length - 1) return;
    if (getMonth(item.create_at) !== getMonth(filterData[i + 1].create_at)) {
      newData.push({
        create_at: item.create_at,
        type: 'separator',
        title: separateDate(item.create_at, filterData[i + 1].create_at),
      });
    }
  });
  return newData;
};

export default addDateSeparator;
