import React from 'react';
import { StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import ProfileHeader from './ProfileHeader';
import References from './References';
import ProfileComplete from './ProfileComplete';
import PostContainer from './PostContainer';
import Spacer from './Spacer';
import fonts from '../assets/fonts/fonts';

const Profile = ({ data }) => {
  const { title, description, profile_complete } = data?.content_object;
  return (
    <PostContainer>
      <ProfileHeader title={title} date={data?.create_at} />
      <Spacer height={scale(10)} />
      <Text style={styles.textInfo}>{description} </Text>
      <Spacer height={scale(19)} />
      <ProfileComplete complete={profile_complete} />
      <Spacer height={scale(20)} />
      <References />
      <Spacer height={scale(20)} />
      <Text style={styles.text}>View my profile</Text>
    </PostContainer>
  );
};

Profile.propTypes = {
  data: PropTypes.shape({
    content_object: PropTypes.shape({
      description: PropTypes.string,
      profile_complete: PropTypes.number,
      title: PropTypes.string,
    }),
    create_at: PropTypes.string,
  }),
};

Profile.defaultProps = {
  data: {
    content_object: {
      description: '',
      profile_complete: 0.25,
      title: '',
    },
    create_at: '',
  },
};

const styles = StyleSheet.create({
  text: {
    ...fonts.t8,
    color: colors.today,
  },
  textInfo: {
    ...fonts.t6,
    lineHeight: scale(17),
  },
});

export default Profile;
