import images from './src/constants/images';

const data = [
  {
    id: 1,
    create_at: '2018-08-28T09:00:00.000Z',
    type: 'rent_item',
    content_object: {
      category: 'Viewing',
      status: 1,
      apartment: {
        images: [images.houseImage, images.reminderBackground, images.houseImage],
        apartment_description: 'Double Bedroom',
        price: 1250,
        price_description: 'pmc inc. bills',
        price_currency: '£',
        address: '58 West Parade, London',
        appartment_options: ['2 beds', '1 bathroom', '53 Wks Contract'],
        landlord: {
          id: 123,
          avatar: images.customer,
        },
        customers: [
          {
            id: 124,
            avatar: images.customer2,
          },
          {
            id: 125,
            avatar: images.customer3,
          },
        ],
      },
    },
  },
  {
    id: 2,
    create_at: '2018-09-02T12:00:00.000Z',
    type: 'feedback',
    content_object: {
      user: {
        id: 126,
        avatar: 'link to avatar',
        position: 'Plumber',
      },
      issue: {
        text: 'Resolved the maintenance issue.',
        status: 1,
      },
    },
  },
  {
    id: 3,
    create_at: '2018-09-03T09:00:00.000Z',
    type: 'profile',
    content_object: {
      title: 'Your renter profile is 25% complete',
      description:
        'There are 13.9 million renters in the UK. Stand out from the crowd by building your rental profile today.',
      profile_complete: 0.25,
    },
  },
  {
    id: 4,
    create_at: '2018-09-04T12:00:00.000Z',
    type: 'payment',
    content_object: {
      title: 'Payment',
      content: 'Rent Payment Due',
      price: 1250,
      price_currency: '£',
    },
  },
  {
    id: 5,
    create_at: '2018-09-05T09:00:00.000Z',
    type: 'rent_item',
    content_object: {
      category: 'Check-in',
      status: null,
      apartment: {
        images: [images.houseImage, images.reminderBackground, images.houseImage],
        apartment_description: 'Double Bedroom',
        price: 1250,
        price_description: 'pmc inc. bills',
        price_currency: '£',
        address: '58 West Parade, London',
        appartment_options: ['2 beds', '1 bathroom', '53 Wks Contract'],
        landlord: {
          id: 123,
          avatar: images.customer,
        },
        customers: [
          {
            id: 124,
            avatar: images.customer2,
          },
          {
            id: 125,
            avatar: images.customer3,
          },
        ],
      },
    },
  },
  {
    id: 6,
    create_at: '2018-09-17T09:00:30.632Z',
    type: 'house_inspection',
    content_object: {
      title: 'House Inspection',
      content: '58 West Parade, London',
      landlord: {
        id: 123,
      },
    },
  },
  {
    id: 7,
    create_at: '2018-09-17T12:00:30.632Z',
    type: 'manager_catch_up',
    content_object: {
      title: 'Manager Catch up',
    },
  },
  {
    id: 8,
    create_at: '2018-12-10T12:00:00.000Z',
    type: 'feedback',
    content_object: {
      user: {
        id: 126,
        avatar: 'link to avatar',
        position: 'Plumber',
      },
      issue: {
        text: 'Resolved the maintenance issue.',
        status: 1,
      },
    },
  },
  {
    id: 9,
    create_at: '2019-06-03T12:00:00.000Z',
    type: 'reminder',
    content_object: {
      title: 'Your contract ends in 3 months.',
    },
  },
  {
    id: 10,
    create_at: '2019-09-05T09:00:00.000Z',
    type: 'rent_item',
    content_object: {
      category: 'Check-out',
      status: 1,
      apartment: {
        images: [images.houseImage, images.reminderBackground, images.houseImage],
        apartment_description: 'Double Bedroom',
        price: 1250,
        price_description: 'pmc inc. bills',
        price_currency: '£',
        address: '58 West Parade, London',
        appartment_options: ['2 beds', '1 bathroom', '53 Wks Contract'],
        landlord: {
          id: 123,
          avatar: images.customer,
        },
        customers: [
          {
            id: 124,
            avatar: images.customer2,
          },
          {
            id: 125,
            avatar: images.customer3,
          },
        ],
      },
    },
  },
];

export default data;
