import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const baseWidth = 375;

export const scale = size => (width / baseWidth) * size;
export const Scale = (size, factor = 0.5) => size + (scale(size) - size) * factor;
