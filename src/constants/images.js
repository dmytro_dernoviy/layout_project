import dashboardIcon from '../assets/img/dashboard-icon.png';
import findIcon from '../assets/img/find-icon.png';
import activityIcon from '../assets/img/activity-icon.png';
import messageIcon from '../assets/img/message-icon.png';
import profileIcon from '../assets/img/profile-icon.png';
import arrowLeft from '../assets/img/arrow-left.png';
import arrowRight from '../assets/img/arrow-right.png';
import clockIcon from '../assets/img/clock-icon.png';
import checkIcon from '../assets/img/check-icon.png';
import houseImage from '../assets/img/house-image.png';
import point from '../assets/img/point.png';
import customer from '../assets/img/customer.png';
import customer2 from '../assets/img/customer-2.png';
import customer3 from '../assets/img/customer-3.png';
import avatarPlumber from '../assets/img/avatar-plumber.png';
import emptyStar from '../assets/img/empty-raiting-star.png';
import fullStar from '../assets/img/full-raiting-star.png';
import portmone from '../assets/img/portmone.png';
import alert from '../assets/img/alert.png';
import orangeArrow from '../assets/img/orange-arrow-right.png';
import reminderBackground from '../assets/img/reminder-background.png';

const images = {
  dashboardIcon,
  findIcon,
  activityIcon,
  messageIcon,
  profileIcon,
  arrowLeft,
  arrowRight,
  clockIcon,
  checkIcon,
  houseImage,
  point,
  customer,
  customer2,
  customer3,
  avatarPlumber,
  emptyStar,
  fullStar,
  portmone,
  alert,
  orangeArrow,
  reminderBackground,
};

export default images;
