import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import Clock from './Clock';
import colors from '../constants/colors';
import PostContainer from './PostContainer';
import Spacer from './Spacer';
import MessageButton from './MessageButton';
import fonts from '../assets/fonts/fonts';

const Reminder2 = ({ data }) => {
  const { title, content } = data?.content_object;
  return (
    <PostContainer>
      <View style={styles.header}>
        <Text style={styles.title}>{title}</Text>
        <Clock time={data?.create_at} />
      </View>
      <Spacer height={scale(10)} />
      <Text style={styles.info}>{content}</Text>
      <MessageButton widthButton={scale(160)} />
    </PostContainer>
  );
};

Reminder2.propTypes = {
  data: PropTypes.shape({
    content_object: PropTypes.shape({
      title: PropTypes.string,
      content: PropTypes.string,
    }),
    create_at: PropTypes.string,
  }),
};

Reminder2.defaultProps = {
  data: {
    content_object: {
      title: '',
      content: '',
    },
    create_at: '',
  },
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    ...fonts.t5,
  },
  info: {
    ...fonts.t4,
    color: colors.info,
  },
});

export default Reminder2;
