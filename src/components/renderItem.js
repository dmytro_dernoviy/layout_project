import React from 'react';
import SlidePost from './SlidePost';
import EventBlock from './EventBlock';
import FeedBack from './FeedBack';
import Profile from './Profile';
import Reminder1 from './Reminder1';
import Reminder2 from './Reminder2';
import Reminder4 from './Reminder4';
import Reminder3 from './Reminder3';
import MonthSeparator from './MonthSeparator';

const renderItemByType = data => {
  const { type, create_at } = data;
  const items = {
    rent_item: <SlidePost data={data} />,
    feedback: <FeedBack data={data} />,
    profile: <Profile data={data} />,
    payment: <Reminder1 content={data?.content_object} date={create_at} />,
    house_inspection: <Reminder2 data={data} />,
    reminder: <Reminder3 data={data?.content_object} />,
    manager_catch_up: <Reminder4 data={data} />,
    separator: <MonthSeparator title={data?.title} />,
  };
  return type === 'separator' ? (
    items.separator
  ) : (
    <EventBlock renderItem={items[type]} date={create_at} />
  );
};

export default renderItemByType;
