import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import UserInfo from './UserInfo';
import CheckedIcon from './CheckIcon';
import colors from '../constants/colors';
import Spacer from './Spacer';
import fonts from '../assets/fonts/fonts';

const FeedBackHeader = ({ position, issue, date }) => (
  <View style={styles.container}>
    <UserInfo position={position} date={date} />
    <Spacer height={scale(16)} />
    <View style={styles.text}>
      <Text style={styles.info}>{issue}</Text>
      <CheckedIcon />
    </View>
  </View>
);

FeedBackHeader.propTypes = {
  position: PropTypes.string,
  issue: PropTypes.string,
  date: PropTypes.string,
};

FeedBackHeader.defaultProps = {
  position: '',
  issue: '',
  date: '',
};

const styles = StyleSheet.create({
  container: {
    width: scale(270),
    paddingTop: scale(17),
    paddingLeft: scale(20),
    paddingRight: scale(20),
    paddingBottom: scale(20),
    backgroundColor: colors.postBackground,
    justifyContent: 'space-between',
  },
  text: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: scale(17),
  },
  info: {
    ...fonts.t5,
    color: colors.resolve,
  },
});

export default FeedBackHeader;
