export const validateData = obj => {
  const { toString } = {};
  const validObj = {
    id: 1,
    create_at: '',
    type: '',
    content_object: {},
  };

  for (const p in validObj) {
    if (obj.hasOwnProperty(p) !== validObj.hasOwnProperty(p)) return false;
    if (toString.call(obj[p]) !== toString.call(validObj[p])) return false;
  }
  return true;
};

export const validate = mokedData => {
  let valid = false;
  for (let i = 0; i <= mokedData.length - 1; i += 1) {
    const isValid = validateData(mokedData[i] || {});
    if (typeof mokedData[i] === 'object' && mokedData[i] !== null && isValid) {
      valid = true;
    } else {
      valid = false;
      break;
    }
  }
  return valid;
};
