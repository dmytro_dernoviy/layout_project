import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';

const Address = ({ address }) => <Text style={styles.text}>{address}</Text>;

const styles = StyleSheet.create({
  text: {
    ...fonts.t7,
    color: colors.address,
    lineHeight: scale(22),
  },
});

Address.propTypes = {
  address: PropTypes.string,
};

Address.defaultProps = {
  address: '',
};

export default Address;
