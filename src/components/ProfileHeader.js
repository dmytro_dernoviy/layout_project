import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Clock from './Clock';
import colors from '../constants/colors';
import { scale } from '../utilites/scale';
import fonts from '../assets/fonts/fonts';

const ProfileHeader = ({ title, date }) => (
  <View style={styles.container}>
    <View style={styles.textContainer}>
      <Text style={styles.text}>{title}</Text>
    </View>
    <Clock time={date} />
  </View>
);

ProfileHeader.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
};

ProfileHeader.defaultProps = {
  title: '',
  date: '',
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: scale(30),
  },
  text: {
    ...fonts.t5,
    color: colors.today,
  },
  textContainer: {
    width: scale(140),
  },
});

export default ProfileHeader;
