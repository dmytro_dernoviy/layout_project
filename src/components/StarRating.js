import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import StarRating from 'react-native-star-rating';
import { scale } from '../utilites/scale';
import images from '../constants/images';

class StarRaiting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 4,
    };
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  render() {
    const { starCount } = this.state;
    return (
      <StarRating
        containerStyle={styles.container}
        disabled={false}
        maxStars={5}
        rating={starCount}
        starStyle={styles.img}
        selectedStar={rating => this.onStarRatingPress(rating)}
        fullStar={images.fullStar}
        emptyStar={images.emptyStar}
      />
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: scale(16),
    height: scale(16),
  },
  container: {
    width: scale(125),
  },
});

export default StarRaiting;
