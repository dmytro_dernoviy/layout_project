import React from 'react';
import { Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import images from '../constants/images';

const TabImage = ({ src }) => <Image source={src} style={styles.img} />;

TabImage.propTypes = {
  src: PropTypes.number,
};

TabImage.defaultProps = {
  src: images.dashboardIcon,
};

const styles = StyleSheet.create({
  img: {
    height: scale(15),
    width: scale(15),
  },
});

export default TabImage;
