import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';
import colors from '../constants/colors';
import Clock from './Clock';
import CheckedIcon from './CheckIcon';
import fonts from '../assets/fonts/fonts';

const SlidePostHeader = ({ data }) => {
  const { category } = data?.content_object;
  return (
    <View style={styles.container}>
      <View style={styles.status}>
        <Text style={styles.text}>{category}</Text>
        {category === 'Viewing' ? <CheckedIcon /> : null}
      </View>
      <Clock time={data?.create_at} />
    </View>
  );
};

SlidePostHeader.propTypes = {
  data: PropTypes.shape({
    create_at: PropTypes.string,
    content_object: PropTypes.shape({
      category: PropTypes.string,
    }),
  }),
};

SlidePostHeader.defaultProps = {
  data: {
    create_at: '',
    content_object: {
      category: '',
    },
  },
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  status: {
    width: scale(100),
    height: scale(15),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: scale(36),
  },
  text: {
    color: colors.resolve,
    ...fonts.t5,
  },
});

export default SlidePostHeader;
