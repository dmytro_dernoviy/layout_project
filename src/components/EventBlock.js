import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import { getDay } from '../utilites/getDate';
import Spacer from './Spacer';
import { scale } from '../utilites/scale';
import fonts from '../assets/fonts/fonts';
import colors from '../constants/colors';
import { compareDate } from '../utilites/compareDate';

const EventBlock = ({ renderItem, date }) => {
  const textStyleByDate = day => {
    const eventsDate = compareDate(day);
    const style = {
      'Past events': styles.pastStyle,
      Today: styles.todayStyle,
    };
    return style[eventsDate];
  };

  return (
    <View>
      <Spacer height={scale(20)} />
      <View style={styles.container}>
        <View>
          <Text style={[styles.dayText, textStyleByDate(date)]}>{getDay(date).dayNumber}</Text>
          <Text style={[styles.weekDay, textStyleByDate(date)]}>{getDay(date).dayName}</Text>
        </View>
        {renderItem}
      </View>
    </View>
  );
};

EventBlock.propTypes = {
  date: PropTypes.string,
  renderItem: PropTypes.node,
};

EventBlock.defaultProps = {
  date: '',
  renderItem: React.node,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dayText: {
    ...fonts.t3,
  },
  weekDay: {
    ...fonts.t4,
  },
  todayStyle: {
    color: colors.today,
  },
  pastStyle: {
    color: colors.days,
    opacity: 0.5,
  },
});

export default EventBlock;
