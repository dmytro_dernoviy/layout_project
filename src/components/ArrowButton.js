import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from '../utilites/scale';

const ArrowButton = ({ image, pressFunc }) => (
  <TouchableOpacity
    onPress={() => pressFunc()}
    hitSlop={{ top: 40, bottom: 40, left: 40, right: 40 }}
  >
    <Image source={image} style={styles.img} />
  </TouchableOpacity>
);

ArrowButton.propTypes = {
  image: PropTypes.number.isRequired,
  pressFunc: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  img: {
    width: scale(5),
    height: scale(8),
  },
});

export default ArrowButton;
