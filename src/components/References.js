import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import { scale } from '../utilites/scale';
import images from '../constants/images';
import colors from '../constants/colors';
import fonts from '../assets/fonts/fonts';

const References = () => (
  <View style={styles.container}>
    <View style={styles.wrapper}>
      <View style={styles.images}>
        <Image source={images.portmone} style={styles.portmone} />
        <Image source={images.alert} style={styles.alert} />
      </View>
      <Text style={styles.text}>Landlord References</Text>
      <Image source={images.orangeArrow} style={styles.arrow} />
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: scale(70),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: scale(5),
    shadowColor: colors.shadowRaiting,
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 33,
    elevation: 1,
  },
  wrapper: {
    width: scale(190),
    height: scale(30),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  images: {
    width: scale(28),
    height: scale(22),
    position: 'relative',
  },
  portmone: {
    width: '100%',
    height: '100%',
  },
  alert: {
    width: scale(11),
    height: scale(11),
    position: 'absolute',
    bottom: scale(-4),
    right: scale(-4),
  },
  arrow: {
    width: scale(4),
    height: scale(8),
  },
  text: {
    alignSelf: 'flex-start',
    ...fonts.t5,
  },
});

export default References;
