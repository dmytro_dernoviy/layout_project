/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import TabNavigator from './src/navigation/TabNavigator';

const App = () => <TabNavigator />;

export default App;
